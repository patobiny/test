import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CommentsService } from '../comments.service';


@Component({
  selector: 'app-comments',
  templateUrl: './comments.component.html',
  styleUrls: ['./comments.component.css']
})
export class CommentsComponent implements OnInit {
postId;
comments$;

  constructor(public commentsService:CommentsService, public router:Router) { }

  
  ngOnInit(): void {
    this.comments$=this.commentsService.backComment();
  }

}
