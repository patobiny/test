import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class DataBaseService {
  
  postsCollection:AngularFirestoreCollection;
  userCollection:AngularFirestoreCollection=this.db.collection('users');

  getPosts(userId):Observable<any[]>{
    this.postsCollection = this.db.collection(`users/${userId}/posts`);
    return this.postsCollection.snapshotChanges().pipe(
      map(
        collection =>collection.map(
          document=> {
            const data = document.payload.doc.data();
            data.id = document.payload.doc.id;
            return data; 
          }
        )  
     ))  
  }


  deletePost(userId:string, id:string){
    this.db.doc(`users/${userId}/posts/${id}`).delete();
     
  }
  addPost(userId:string, id:number, title:string, body:string ){
    const post = {id:id, title:title,body:body};
    this.userCollection.doc(userId).collection('posts').add(post);
  }

  public updateLikes(userId:string, id:string, likes:number){
    this.db.doc(`users/${userId}/posts/${id}`).update(
      {
       likes:likes
      
      }
    )

  }

  public updatePrediction(userId:string, id:string, prediction:string){
    this.db.doc(`users/${userId}/posts/${id}`).update(
      {
       prediction:prediction
      
      }
    )

  }


  constructor(private db:AngularFirestore) { }
}