import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import {AngularFireAuth} from '@angular/fire/auth';
import { Observable } from 'rxjs';
import { User } from './interfaces/user';


@Injectable({
  providedIn: 'root'
})
export class AuthService {
  user:Observable<User | null>; 
  
email:string;


  SignUp(email:string, password:string){
    return this.afAuth.createUserWithEmailAndPassword(email,password);
  }
  
  
  login(email:string, password:string){
    
    return this.afAuth
        .signInWithEmailAndPassword(email,password).then(
          res =>{
            this.router.navigate(['/welcome']);
            this.email=email;
             console.log(this.email);
            return this.email ;
          }
        )  
  }

  logout(){
    this.afAuth.signOut().then(
      res =>{
        this.router.navigate(['/welcome']); 
      }
    ) 
  }

  getUser():Observable<User | null>  {
    return this.user; 
  }

  constructor(private afAuth:AngularFireAuth, private router:Router) { 
    this.user = this.afAuth.authState; 
  }

}
