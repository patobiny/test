import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit {
email=this.auth.email;
password;
  constructor(private auth:AuthService,private router:Router) { }

  ngOnInit(): void {
    this.auth.login(this.email,this.password).then(
      res =>{
        console.log(res);
        
      }
    )
  }

}
