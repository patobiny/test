import { ClassifyService } from './../classify.service';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Post } from '../interfaces/post';
import { BlogService } from '../blog.service';
import { CommentsService } from '../comments.service';
import { DataBaseService } from '../data-base.service';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-posts',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.css']
})
export class BlogComponent implements OnInit {

posts$:Observable<Post>
comments$;

posts:Post[];
userId:string;
//addCustomerFormOpen= false;

title:string;
body:string;
id:number;
saved=[];
class=[];

add(id,title, body){
  this.db.addPost(this.userId,id, title,body);
}



  showComments(postid){
    this.commentsService.getComments(postid);
     
        
      this.router.navigate(['/comments']);
    
  
  }
  constructor(private blogService:BlogService, private commentsService:CommentsService,private router:Router,private db:DataBaseService,public authService:AuthService, public classifyService:ClassifyService) { }


  prediction=null;


  classify(title,body){
   
    this.classifyService.classify(title,body).subscribe(
      res => {
        console.log(res);
      if(res>5){
        this.prediction='Yes'
      }else{
        this.prediction='No'
      }return this.prediction;
     }
      )
  }

  


  ngOnInit(): void {
    this.posts$ = this.blogService.getPosts();
    this.authService.getUser().subscribe(
      user => {
                this.userId = user.uid;
    })


}
}