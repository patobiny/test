import { DataBaseService } from './../data-base.service';
import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { BlogService } from '../blog.service';
import { Post } from '../interfaces/post';

@Component({
  selector: 'app-saved-posts',
  templateUrl: './saved-posts.component.html',
  styleUrls: ['./saved-posts.component.css']
})
export class SavedPostsComponent implements OnInit {

  posts$; 
  posts:Post[];
  userId:string;


  delete(id:string){
    this.db.deletePost(this.userId, id);
    console.log(id);
  }

  likes(likes,id){
    if(likes==null){
      likes=0
    };this.db.updateLikes(this.userId,id, likes+1);
  }

  constructor(private db:DataBaseService, public authService:AuthService) { }

  ngOnInit(): void {
    this.authService.getUser().subscribe(
      user => {
        this.userId = user.uid;
        this.posts$ = this.db.getPosts(this.userId);
        
    })
  }

}
